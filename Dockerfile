FROM python:3.10 as docs

ARG ROOT=/home/seta
RUN useradd seta

WORKDIR $ROOT

COPY ./requirements.txt ./requirements.txt
RUN python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir -r ./requirements.txt && \
    python3 -m pip check

COPY . $ROOT

RUN ["mkdocs", "build"]

FROM nginx:1.25.4
COPY --from=docs /home/seta/site /usr/share/nginx/html