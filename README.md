# docs-app

## Web Application
SeTA documentation with mkdocs

## Container Registry
If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username and password. 
If you have Two-Factor Authentication enabled, use a [Personal Access Token](https://code.europa.eu/help/user/profile/personal_access_tokens) instead of a password.

Login with Personal Access Token:
```
docker login code.europa.eu:4567
```

### Build Docs Image

Build production image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/frontend/docs-app:{$VERSION} .
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/frontend/docs-app:{$VERSION}
```

### Build Image for Development 

First, clone `code.europa.eu/seta/v3/composer/docker-compose` project and run `docker compose up` - check project documentation

Create the `.env` file in `./compose-dev` folder and run:

```
docker compose build
docker compose up
```

The up command recreates the existing 'doc' and 'nginx' containers.